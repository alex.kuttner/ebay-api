<?php
require_once 'Call.php';

class Base {

    public $Call;
    public $Configure;

    public function __construct($callName)
    {
        $this->Call = new Call($callName);
    }

}