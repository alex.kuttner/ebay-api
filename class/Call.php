<?php require_once 'contracts/Configure.php';

class Call extends Configure
{
    private $Config;

    public function __construct($callName)
    {
        $this->Config = new Configure($callName);
        /**
         * First thing we do is configure the API then we can make a Call
         */
        parent::__construct($callName);
    }

    /**
     * This was just used for testing
     * This will show seller items that are active for this account from 2015-07-12 to 2015-08-26
     */
    public function callGetSellerList()
    {
        $xmlRequest = <<<BODY
        <?xml version="1.0" encoding="utf-8"?>
        <GetSellerListRequest xmlns="urn:ebay:apis:eBLBaseComponents">
          <RequesterCredentials>
            <eBayAuthToken>$this->authToken</eBayAuthToken>
          </RequesterCredentials>
          <ErrorLanguage>en_US</ErrorLanguage>
          <WarningLevel>High</WarningLevel>
          <GranularityLevel>Coarse</GranularityLevel>
          <StartTimeFrom>2015-07-12T21:59:59.005Z</StartTimeFrom>
          <StartTimeTo>2015-08-26T21:59:59.005Z</StartTimeTo>
          <IncludeWatchCount>true</IncludeWatchCount>
          <Pagination>
            <EntriesPerPage>2</EntriesPerPage>
          </Pagination>
          <DetailLevel>ReturnAll</DetailLevel>
        </GetSellerListRequest>
BODY;

        $response = parent::makeCurlRequest($xmlRequest);

        return $response;
    }
}