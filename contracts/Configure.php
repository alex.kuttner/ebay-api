<?php
class Configure
{
    //If equal to true then you are in Sandbox mode
    //Otherwise false equals Production
    private $sandbox;

    //All Trading API requests that you execute must be sent to the Sandbox or Production Gateway for the API
    private $endpoint;

    //Keys and Token
    private $apiDevName;
    private $apiAppName;
    private $apiCertName;
    public $authToken;

    //Headers
    private $headers;

    //The Compatability Level is the eBay release version that the application supports
    private $compatabilityLevel;

    //The callName value must match the request name. For example, if you are using AddItemRequest, specify "AddItem" (without "Request") in this call name header.
    private $callName;

    //This is the site ID that the items are on 15 is for AU
    private $siteID;

    public function __construct($callName, $compatabilityLevel = '929', $sandbox = true, $siteID = '15')
    {
        $this->callName = $callName;
        $this->sandbox = $sandbox;
        $this->siteID = $siteID;
        $this->compatabilityLevel = $compatabilityLevel;

        //First we need to set the keys
        $this->setKeys();

        //Secondly we need to set the headers
        $this->setHeaders();
    }

    public function setKeys()
    {
        /**
         * There are different keys for sandbox and production so set them here - THESE WILL NOT WORK THEY ARE EXAMPLE USE YOUR OWN !!!
         */
        $this->endpoint = ($this->sandbox) ? 'https://api.sandbosx.ebay.com/ws/api.dll' : 'https://api.ebay.com/ws/api.dll';
        $this->apiDevName = ($this->sandbox) ? "0227esd520-10df21-479d-8ef0-f9e6340dc754" : "0227e520-103421-479d-8ef0-f9e6340dc754";
        $this->apiAppName = ($this->sandbox) ? "t***x40c7-f6307-4d5a5-a4b7-eeb724fc4e1" : "tym3f77e-1183-4f47-bfaf-d2e1c61964d";
        $this->apiCertName = ($this->sandbox) ? "12a8822b-05bd-4ewfa9-a448-a1b1211dea48" : "5bf6Df302e6-29cb-4fde-b5be-9f95dc49f8c1";
        $this->authToken = ($this->sandbox) ? "AgAAAA**AQAAAAA**aAAAAAA**Pk+mVQ**nY+sHZ2DPrBmdj6wVnY+sEZ2PrA2dj6wFk4GhDZiLowudj6x9nY+seQ**gIIDAA**AAMAAA**6i1+J1OJq8gcOLHNoy6Q0lXBJI/8I2ZyBj81St00N1iG6dQ161/8OhIOeSNuscfGj0WF3Stsf3e4yWdbPx0BlTQUIVTFxIt7Y8cR376e8EGHO6wT3LAwsf2PwXtJwaOu1XabUVqzJpVA84W0HdPr2/6X/8vyhIHgoGGYY65fSflEOUB8M8ntR2/8Fux/kXW+tRLckXhoQ7K7QZTurkHIoqB4CSkxL0vPli3PIHrcjIXCAvJTNdyUW1Qc3kNDqxb4GwqJ9BIrMKyG+opMiYEC+keDppdk+BytLcXJsSBopCqu3ojP4D9Dnkctcz0I4Hry+tR3BKz21ZZTgiDs7MvBGLhtfHb9pkAzKdTeBxdeDMsTrQFS5HoKqHXi9pAASCOBdg12riZbQeuWDyh6AcgylqrJ7D353X0lgh0wwr/FvK30Bfrra4qDftj2lZsqacb+nkpm5qKKWoxROsd0gwNCyA6d84Fk/Ohvt0inLD2cpwMfe4pqe3qDtVqgUQlkcCgt9YYMTrAbRcux/Kcqx1PDMQD/heBm78bHSRiRwSb/wSWx5DL2i5Uspu9yqPA561b0PQ5OtX+Yvcbem+P5OZwg9yOsa44LpnSSoRxiO9DvdsCjpHpZodqItM27ZC2zJnd76q1ex6GZ0SfYMgHlypO8OdPEq9J2kqQOo0ZmqOAxPyjvnaNZTZtXq84wNEySh6kS2b+ko2jkCoumbF/IJKXIhKvaVgOLW7VJT1CrxbWfm5SN9+FdPtRL6VPzTv2Esbi+RyDYFZXc" : "";
    }

    public function setHeaders()
    {
        /**
         * To properly route an XML API request to the appropriate destination within eBay
         *
         * Required Headers:
         * X-EBAY-API-COMPATIBILITY-LEVEL, X-EBAY-API-CALL-NAME, X-EBAY-API-SITEID
         */
        $this->headers = array(
            'Content-Type:text/xml',
            'X-EBAY-API-COMPATIBILITY-LEVEL: '.$this->compatabilityLevel,
            'X-EBAY-API-DEV-NAME: '.$this->apiDevName,
            'X-EBAY-API-APP-NAME: '.$this->apiAppName,
            'X-EBAY-API-CERT-NAME :'.$this->apiCertName,
            'X-EBAY-API-CALL-NAME: '.$this->callName,
            'X-EBAY-API-SITEID: '.$this->siteID
        );
    }

    public function makeCurlRequest($xmlRequest)
    {
        /**
         * At this point we our headers and xml ready
         * Send the request to eBay using CURL
         */
        $connection = curl_init();
        curl_setopt($connection, CURLOPT_URL, $this->endpoint);
        curl_setopt($connection, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($connection, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($connection, CURLOPT_HTTPHEADER, $this->headers);
        curl_setopt($connection, CURLOPT_POST, 1);
        curl_setopt($connection, CURLOPT_POSTFIELDS, $xmlRequest);
        curl_setopt($connection, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($connection);

        //Close the connection
        curl_close($connection);

        //Return XML response
        return $response;
    }
}
